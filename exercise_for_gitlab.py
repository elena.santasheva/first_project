n = 'continue'

while n == 'continue':
    n = input('Enter exit, exit-no-print, no-print: ')  # n = 'exit' or n = 'exit-no-print' or n = 'no-print' or n = ?
    if n == 'exit':
        print('Exit')
    elif n == 'exit-no-print':
        break
    elif n == 'no-print':
        n = 'continue'
    else:
        n = 'continue'
else:
    print('Done!')

